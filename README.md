# Software Engineering1 Currency Converter

Our first Java programming project is this currency converter. 

The currency converter works like this:
First you choose a currency you want to buy, then one you want to sell and finally the amount.

The interface is designed in such a way that the currencies you want to buy and sell are displayed at the top. Below that, there are three options that the programme executes.

As this was our first program we programmed, we had some logic errors and difficulties with the unit tests in the beginning. But we were able to solve these together. 


**Team members:**  
> Sinem B.  
Katharina S.   
Annalena H.  
