package org.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

public class CurrencyConverter {
    //Die currency sdr werte sind vom 4.juni 2021

    private String[] currencies = {};
    private double[] sdr = {};

    //ist auf -1, da wir ja noch keine Währung ausgesucht haben und das Array ab index 0 startet
    private int currencyBuy = -1;
    private int currencySell = -1;

    private double inputamount = 0;
    private double outputamount = 0;

    //für die tests
    public String[] getCurrencies() { return currencies; }

    public void setCurrencies(String[] currencies) { this.currencies = currencies; }

    public double[] getSdr() { return sdr; }

    public void setSdr(double[] sdr) { this.sdr = sdr; }

    public int getCurrencyBuy() { return currencyBuy; }

    public void setCurrencyBuy(int currencyBuy) { this.currencyBuy = currencyBuy; }

    public void setCurrencySell(int currencySell) { this.currencySell = currencySell; }

    public double getInputamount() { return inputamount; }

    public void setInputamount(double inputamount) { this.inputamount = inputamount; }

    public double getOutputamount() { return outputamount;}


    private static Scanner SCAN;

    private final String CURRENCY_FILE = "./currencyFile.csv";

    public void setSCAN(InputStream inputStream) {
        this.SCAN = new Scanner(inputStream);
    }



    /**
     * readfile:
     * - Die Namen und die sdr Werte werden in zwei verschiedenen Arrays gspeichert
     */
    public void readFile(String filename) {

        String line;
        String splitBy = ";";

        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            while ((line = br.readLine()) != null) {
                String[] contentLineCurrency = line.split(splitBy);
                int arrayLength = currencies.length;
                String[] currenciesNew = Arrays.copyOf(currencies, arrayLength + 1);
                double[] sdrNew = Arrays.copyOf(sdr, arrayLength + 1);
                currenciesNew[arrayLength] = contentLineCurrency[0];
                sdrNew[arrayLength] = Double.parseDouble(contentLineCurrency[1]);
                currencies = currenciesNew;
                sdr = sdrNew;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * selectCurrency:
     * - Ein Array mit 40 Plätzen wird jedes mal initialisiert
     * - 1. For-Schleife: Eingabe einer Währung (!in Großbuchstaben!) oder nur die anfangs Buchstaben
     * - In unserem currencyFound speichern wir ab, wie viele Währungen wir gefunden haben
     * - Die 2.For-Schleife printet nun die currency mit einem "neuem" Index
     * - Danach muss man den "Index" von der Währung angeben die man haben will.
     * - Nicht aufgelistete Zahlen und Währungen werden abgefangen
     */
    public int selectCurrency() {

        int selectedCurrency = -1;

        while (true) {
            // er macht jedesmal ein neues array
            int[] foundCurrenciesBuy = new int[40];
            int count = 0;

            System.out.println("Enter a Currency´s name or part of it (xxx to Exit): ");
            String buyUserInput = SCAN.next();
            if (buyUserInput.equals("xxx")) {
                System.out.println("Exit");
                selectedCurrency = -1;
                break;
            }
            for (int i = 0; i < currencies.length; i++) {
                if (currencies[i].toLowerCase().contains(buyUserInput.toLowerCase())) {
                    foundCurrenciesBuy[count] = i;
                    count++;
                }
            }
            if (count > 0) {
                for (int i = 0; i < count; i++) {
                    System.out.println(i + ":" + currencies[foundCurrenciesBuy[i]]);
                }
                while (true) {

                    System.out.print("Select a currency by index: ");

                    try {
                        String input = SCAN.next();
                        int number = Integer.parseInt(input);
                        if (number >= 0 && number < count) {
                            selectedCurrency = foundCurrenciesBuy[number];
                            break;
                        } else {
                            System.out.println("Error. Wrong input");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            } else {
                System.out.println("This currency is not defined.");
            }
        }
        return selectedCurrency;
    }

    /**
     * currencyChange:
     * - Die eingegebene Summe wird in die gewünschte Währung umgerechnet und ausgegeben
     * - Es wird auf zwei Nachkommastellen gerundet
     * - (Wenn man danach buy, sell oder amount ändert wird ädert sich dementsprechend der output)
     */
    public void currencyChange() {
        if (inputamount >= 0 && currencyBuy >= 0 && currencySell >= 0) {
            double sum = inputamount * sdr[currencyBuy] / sdr[currencySell];
            sum = Math.round(sum * 100.0) / 100.0;
            outputamount = sum;
        }
    }

    /**
     * currencyAmount:
     * - Eingeben eines amount (Summe die umgerechnet werden soll)
     * - Eingaben die keine Zahlen sind werden abgefangen
     */
    public void currencyAmount() {

        while (true) {
            System.out.println("Enter an amount: ");
            String amountUserInput = SCAN.next();

            try {
                inputamount = Double.parseDouble(amountUserInput);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Wrong input format!");
            }
        }
    }

    /**
     * main:
     * - Menü wird ausgeprintet
     * - je nach input wird die jeweilige Funktion aufgerufen
     * - wird von unsere Klasse Driver aufgerufen und ausgeführt
     */
    public void run() {
        // wir rufen readFile() in der main auf, damit currencyBuy in der for Schleife auf die Funktion zugreift
        setSCAN(System.in);
        readFile(CURRENCY_FILE);

        try {
            char valueOptions;

            do {
                if (currencyBuy != -1 && currencySell != -1) {
                    System.out.println("Buying " + inputamount + " of " + currencies[currencyBuy]);
                    System.out.println("Selling " + outputamount + " of " + currencies[currencySell]);

                } else {

                    System.out.print("Currency to buy: ");
                    if (currencyBuy == -1) {
                        System.out.println("not set");
                    } else {
                        System.out.println(currencies[currencyBuy]);
                    }
                    System.out.print("Currency to sell: ");
                    if (currencySell == -1) {
                        System.out.println("not set");
                    } else {
                        System.out.println(currencies[currencySell]);
                    }
                }

                System.out.println("====================================");
                System.out.println("0: Select currency to buy\n1: Select currency to sell\n2: Choose amount to be converted\n");
                System.out.println("Please choose an option (use x to exit): ");

                valueOptions = SCAN.next().charAt(0);

                switch (valueOptions) {
                    case '0':
                        currencyBuy = selectCurrency();
                        currencyChange();
                        break;
                    case '1':
                        currencySell = selectCurrency();
                        currencyChange();
                        break;
                    case '2':
                        currencyAmount();
                        currencyChange();
                        break;
                    case 'x':
                        System.out.println("Exit");
                        break;
                    default:
                        System.err.println("Error. Please enter 0 , 1 or 2.");
                        SCAN.next();
                }
            } while (valueOptions != 'x');
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}