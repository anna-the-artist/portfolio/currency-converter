package org.example;

public class Driver {
    public static void main(String[] args) {

        CurrencyConverter currencyConverter = new CurrencyConverter();

        currencyConverter.run();
    }
}
