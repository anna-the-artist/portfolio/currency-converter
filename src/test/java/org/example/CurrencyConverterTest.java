package org.example;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import java.io.ByteArrayInputStream;


public class CurrencyConverterTest {

    /**
     * testCurrencyChange:
     * - Die Testklasse ruft currencyChange auf und gibt ihm Werte mit
     * - Schaut ob er das richtig umrechnet
     */
    @Test
    public void testCurrencyChange() {
        CurrencyConverter testCurrencyConverter = new CurrencyConverter();

        // Test mit sdr von Euro, U.S. Dollar und Canadian Dollar
        String[] testCurrencies = {"Euro", "US Dollar", "Candian Dollar"};
        double[] testSdr = {1.187430, 1.438810, 1.738660};
        int testCurrencyBuy = 0;
        int testCurrencySell = 1;
        double testInputAmount = 12.34;

        testCurrencyConverter.setCurrencies(testCurrencies);
        testCurrencyConverter.setSdr(testSdr);
        testCurrencyConverter.setCurrencyBuy(testCurrencyBuy);
        testCurrencyConverter.setCurrencySell(testCurrencySell);
        testCurrencyConverter.setInputamount(testInputAmount);

        testCurrencyConverter.currencyChange();
        Assertions.assertEquals(10.18, testCurrencyConverter.getOutputamount());
    }

    /**
     * testReadFile:
     * - Schaut ob er die File richtig einliest
     * - Überprüft ob an der Stelle 0 Euro und der sdr Wert steht.
     * - Überfrüft die länge der File, also z.B. für die sdr Werte befinden sich 3 Werte in der File
     * - --> wenn er es mit 3 schafft gehen wir davon aus das er es auch mit mehr Werte schafft
     */
    @Test
    public void testReadFile() {
        CurrencyConverter testCurrencyConverter = new CurrencyConverter();

        final String FILENAME="currencyTestFile.csv";

        testCurrencyConverter.readFile(FILENAME);
        String[] testCurrencies = testCurrencyConverter.getCurrencies();
        double[] testSdr = testCurrencyConverter.getSdr();

        Assertions.assertEquals("Euro", testCurrencies[0]);
        Assertions.assertEquals(1.187430, testSdr[0]);

        Assertions.assertEquals(3, testCurrencies.length);
        Assertions.assertEquals(3, testSdr.length);
    }

    /**
     * testSelectCurrency:
     * - Prüft am Anfang ob schon eien Währung ausgesucht wurde
     * - Geben ihm die Were mit
     * - Erwarten das er zwei Währungen zur Auswahl gibt
     */
    @Test
    public void testSelectCurrency() {
        CurrencyConverter testCurrencyConverter = new CurrencyConverter();
        final String FILENAME="currencyTestFile.csv";
        testCurrencyConverter.readFile(FILENAME);

        Assertions.assertEquals(3, testCurrencyConverter.getCurrencies().length);
        Assertions.assertEquals(-1, testCurrencyConverter.getCurrencyBuy());

        ByteArrayInputStream testInputStream = new ByteArrayInputStream("dollar\n1\n".getBytes());
        testCurrencyConverter.setSCAN(testInputStream);
        int selectedCurrency = testCurrencyConverter.selectCurrency();
        Assertions.assertEquals(2, selectedCurrency);
    }

    /**
     * testCurrencyAmount:
     * - Geben ihm die Menge mit (Input)
     * - Erwarten das er sie als double abspeichert
     */
    @Test
    public void testCurrencyAmount() {
        CurrencyConverter testCurrencyConverter = new CurrencyConverter();
        ByteArrayInputStream testInputStream = new ByteArrayInputStream("1.23\n".getBytes());
        testCurrencyConverter.setSCAN(testInputStream);
        testCurrencyConverter.currencyAmount();
        Assertions.assertEquals(1.23, testCurrencyConverter.getInputamount());
    }

    /**
     * testCurrencyAmountError:
     * - Wenn man Buchstaben eingibt das es dann eine Fehlermeldung gibt
     */
    @Test
    public void testCurrencyAmountError() {
        CurrencyConverter testCurrencyConverter = new CurrencyConverter();
        ByteArrayInputStream testInputStream = new ByteArrayInputStream("ABC\n1.23\n".getBytes());
        testCurrencyConverter.setSCAN(testInputStream);
        testCurrencyConverter.currencyAmount();
        Assertions.assertEquals(1.23, testCurrencyConverter.getInputamount());
    }
}

